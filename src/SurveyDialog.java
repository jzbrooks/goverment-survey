import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

/**
 * Justin Brooks
 * CS350 Project #5
 * Dialog box frame.
 * If modify is pressed, it reads existing sample and presents data in the dialog.
 * If add is pressed, a blank dialog is created.
 */

public class SurveyDialog extends JDialog implements ActionListener{
    // Labels
    private JLabel NoLabel;
    private JLabel ParticipantLabel;
    private JLabel ZipLabel;
    private JLabel EmploymentLabel;
    private JLabel ImpactLabel;
    private JLabel CauseLabel;
    private JTextField ZipTextField;

    // Employment
    ButtonGroup  buttonGroup;
    JRadioButton federal;
    JRadioButton state;
    JRadioButton privateEmployment;
    JRadioButton selfEmployed;
    JRadioButton unemployed;

    // Impact
    ButtonGroup  buttonGroup2;
    JRadioButton yes;
    JRadioButton no;
    JRadioButton unknown;

    // Cause
    JCheckBox R;
    JCheckBox D;
    JCheckBox O;
    JCheckBox C;

    private JButton okButton;
    private JButton cancelButton;

    private boolean cancelled;
    public boolean isCancelled() { return cancelled; }
    CSample sample;
    public CSample getCSample () {
        return sample;
    }

    public SurveyDialog(JFrame owner, String title, CSample initSample) {
        super(owner, title, true);

        Container c = getContentPane();
        c.setLayout(null);

        NoLabel = new JLabel("Participant No.");
        NoLabel.setFont(new Font("Arial", Font.BOLD, 14));
        NoLabel.setForeground(Color.BLUE);
        NoLabel.setSize( 150, 30 );
        NoLabel.setLocation( 20,20 );
        c.add(NoLabel);

        ParticipantLabel = new JLabel(initSample.getParticipantNo());
        ParticipantLabel.setFont(new Font("Arial", Font.BOLD, 14));
        ParticipantLabel.setForeground(Color.MAGENTA);
        ParticipantLabel.setSize( 150, 30);
        ParticipantLabel.setLocation( 130, 20);
        c.add(ParticipantLabel);

        ZipLabel = new JLabel("Zip Code");
        ZipLabel.setFont(new Font("Arial", Font.BOLD, 14));
        ZipLabel.setForeground(Color.BLUE);
        ZipLabel.setSize(150, 50);
        ZipLabel.setLocation(20, 50);
        c.add(ZipLabel);

        ZipTextField = new JTextField(initSample.getZipCode());
        ZipTextField.setSize( 75, 20 );
        ZipTextField.setLocation( 85 , 65 );
        c.add(ZipTextField);

        // Employment Question
        EmploymentLabel = new JLabel("Employment Type");
        EmploymentLabel.setFont(new Font("Arial", Font.BOLD, 14));
        EmploymentLabel.setForeground(Color.BLUE);
        EmploymentLabel.setSize( 150, 50 );
        EmploymentLabel.setLocation( 20, 90 );
        c.add(EmploymentLabel);

        buttonGroup = new ButtonGroup();
        federal = new JRadioButton("Federal", initSample.employment==0);
        state = new JRadioButton("State/Local", initSample.employment==5);
        privateEmployment = new JRadioButton("Private", initSample.employment==2);
        selfEmployed = new JRadioButton("Self-Employed", initSample.employment==3);
        unemployed = new JRadioButton("Unemployed", initSample.employment==4);
        federal.setLocation(20, 135);
        federal.setSize(75, 20);
        state.setLocation(110, 135);
        state.setSize(100, 20);
        privateEmployment.setLocation(230, 135);
        privateEmployment.setSize(80, 20);
        selfEmployed.setLocation(320, 135);
        selfEmployed.setSize(120, 20);
        unemployed.setLocation(450, 135);
        unemployed.setSize(100, 20);
        buttonGroup.add(federal);
        buttonGroup.add(state);
        buttonGroup.add(privateEmployment);
        buttonGroup.add(selfEmployed);
        buttonGroup.add(unemployed);
        c.add(federal);
        c.add(state);
        c.add(privateEmployment);
        c.add(selfEmployed);
        c.add(unemployed);

        // Impact Question
        ImpactLabel = new JLabel("Does government shutdown impact you?");
        ImpactLabel.setFont(new Font("Arial", Font.BOLD, 14));
        ImpactLabel.setForeground(Color.BLUE);
        ImpactLabel.setSize(400,30);
        ImpactLabel.setLocation(20, 180);
        c.add(ImpactLabel);

        buttonGroup2 = new ButtonGroup();
        yes = new JRadioButton("Yes", initSample.impact==0);
        no = new JRadioButton("No", initSample.impact==3);
        unknown = new JRadioButton("Unknown", initSample.impact==2);
        yes.setSize(100, 20);
        yes.setLocation(20, 215);
        no.setSize(100, 20);
        no.setLocation(170,215);
        unknown.setSize(100, 20);
        unknown.setLocation(320, 215);
        buttonGroup2.add(yes);
        buttonGroup2.add(no);
        buttonGroup2.add(unknown);
        c.add(yes);
        c.add(no);
        c.add(unknown);

        // Blame question
        CauseLabel = new JLabel("Who/What to blame for the government shutdown?");
        CauseLabel.setFont(new Font("Arial", Font.BOLD, 14));
        CauseLabel.setForeground(Color.BLUE);
        CauseLabel.setSize(400,30);
        CauseLabel.setLocation(20,255);
        c.add(CauseLabel);

        R = new JCheckBox("Republicans(R)", (initSample.cause&1)!=0);
        R.setSize(130,20);
        R.setLocation(20, 290);
        D = new JCheckBox("Democrats(D)", (initSample.cause&2)!=0);
        D.setSize(130,20);
        D.setLocation(170, 290);
        O = new JCheckBox("ObamaCare(O)", (initSample.cause&4)!=0);
        O.setSize(130,20);
        O.setLocation(320, 290);
        C = new JCheckBox("Debt Ceiling(C)", (initSample.cause&8)!=0);
        C.setSize(130,20);
        C.setLocation(470, 290);
        c.add(R);
        c.add(D);
        c.add(O);
        c.add(C);

        cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(this);
        cancelButton.setSize( 175, 50 );
        cancelButton.setLocation( 305 , 325 );
        c.add(cancelButton);

        okButton = new JButton("Submit");
        okButton.addActionListener(this);
        okButton.setSize( 175, 50 );
        okButton.setLocation( 110, 325 );
        c.add(okButton);

        setSize( 600, 430 );
        setLocationRelativeTo(owner);
        setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource()==okButton) {
            int survey_number = Integer.parseInt(this.ParticipantLabel.getText());

            String zip_code = ZipTextField.getText();

            int emp = -1;
            if (federal.isSelected()) emp = 0;
            if (state.isSelected()) emp = 5;
            if (privateEmployment.isSelected()) emp = 2;
            if (selfEmployed.isSelected()) emp = 3;
            if (unemployed.isSelected()) emp = 4;

            int imp = -1;
            if (yes.isSelected()) imp = 0;
            if (no.isSelected()) imp = 3;
            if (unknown.isSelected()) imp = 2;

            int cse = 0;
            if (R.isSelected()) cse |= 1;
            if (D.isSelected()) cse |= 2;
            if (O.isSelected()) cse |= 4;
            if (C.isSelected()) cse |= 8;

            sample = new CSample(survey_number, zip_code, emp, imp, cse);

            cancelled = false;
            setVisible(false);
        }

        else if(e.getSource()==cancelButton) {
            cancelled = true;
            setVisible(false);
        }
    }

}

