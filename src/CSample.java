import java.io.Serializable;

/**
 * Justin Brooks
 * CS350 Project #5
 * Describes survey samples
 */

public class CSample implements Serializable {
    private int participantNo;
    private String zipCode;
    int employment;
    int impact;
    int cause;

    public CSample(int srvyNum, String zipCode, int emp, int impct, int cse) {
        this.participantNo = srvyNum;
        this.zipCode = zipCode;
        this.employment = emp;
        this.impact = impct;
        this.cause = cse;
    }
    public String getParticipantNo() {
        return String.format("%08d",participantNo);
    }

    public String getZipCode() {
        return zipCode;
    }

    private String getEmploymentString() {
        switch (this.employment) {
            case 0:
                return "Federal";
            case 5:
                return "State/Local";
            case 2:
                return "Private";
            case 3:
                return "Self-Employed";
            case 4:
                return "Unemployed";
        }
        return " ";
    }

    private String getImpactString() {
        switch (this.impact) {
            case 0:
                return "Yes";
            case 3:
                return "No";
            case 2:
                return "Unknown";
        }
        return " ";
    }

    private String getCauseString() {
        String str = "";
        str += ((this.cause&1)!=0)?"R":"-";
        str += ((this.cause&2)!=0)?"D":"-";
        str += ((this.cause&4)!=0)?"O":"-";
        str += ((this.cause&8)!=0)?"C":"-";

        return str;
    }

    @Override
    public String toString() {
        return String.format("   %08d             %-22s%-29s%-21s%-21s",
                participantNo, zipCode, getEmploymentString(), getImpactString(), getCauseString());
    }
}
