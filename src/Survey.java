import java.awt.event.*;
import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.ArrayList;

/**
 * Justin Brooks
 * CS350 Project #5
 * Main window that collects samples in an orderly way.
 * Add / Modify / Remove / RemoveAll / Save / Open buttons as well.
 * Added overloaded constructor for Survey that accepts a filename
 * and initializes data from it.
 */

public class Survey extends JFrame implements ActionListener {

    private String filename = null;
    private ObjectOutputStream output;
    private ObjectInputStream input;
    // Label for main window columns
    JLabel column1 = null;
    JLabel column2 = null;
    JLabel column3 = null;
    JLabel column4 = null;
    JLabel column5 = null;
    // Collection of surveys
    static ArrayList<CSample> sampleArray;
    private DefaultListModel surveys;
    JList sampleList;

    // Other GUI Components
    JScrollPane scrollPane = null;
    JButton bnAdd = null;
    JButton bnModify = null;
    JButton bnRemove = null;
    JButton bnRemoveAll = null;
    JButton save = null;
    JButton open = null;
    // Survey Number (insures unique number for each survey)
    int surveyNumber;

    // Constructor for main window
    public Survey() {
        super("Survey on Government Shutdown");

        Container c = getContentPane();
        c.setLayout(null);

        // Column Labels
        column1 = new JLabel("Participant No.");
        column1.setFont(new Font("Arial", Font.BOLD, 14));
        column1.setForeground(Color.BLUE);
        column1.setLocation(15, 8);
        column1.setSize(150, 18);
        c.add(column1);

        column2 = new JLabel("Zip Code");
        column2.setFont(new Font("Arial", Font.BOLD, 14));
        column2.setForeground(Color.BLUE);
        column2.setLocation(180, 8);
        column2.setSize(150, 18);
        c.add(column2);

        column3 = new JLabel("Employment");
        column3.setFont(new Font("Arial", Font.BOLD, 14));
        column3.setForeground(Color.BLUE);
        column3.setLocation(333, 8);
        column3.setSize(150, 18);
        c.add(column3);

        column4 = new JLabel("Impact");
        column4.setFont(new Font("Arial", Font.BOLD, 14));
        column4.setForeground(Color.BLUE);
        column4.setLocation(535,8);
        column4.setSize(150,18);
        c.add(column4);

        column5 = new JLabel("Cause");
        column5.setFont(new Font("Arial", Font.BOLD, 14));
        column5.setForeground(Color.BLUE);
        column5.setLocation(680,8);
        column5.setSize(150,18);
        c.add(column5);

        // Set up display area
        sampleArray = new ArrayList<CSample>();
        surveys = new DefaultListModel();
        sampleList = new JList(surveys);
        sampleList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION );
        sampleList.setFont(new Font("Monospaced", Font.PLAIN, 12));
        scrollPane = new JScrollPane(sampleList);
        scrollPane.setSize(765, 350);
        scrollPane.setLocation(10, 30);
        c.add( scrollPane );

        // Buttons
        bnAdd = new JButton("Add");
        bnAdd.setSize(160,50);
        bnAdd.setLocation(100, 390);
        bnAdd.addActionListener(this);
        c.add(bnAdd);

        bnModify = new JButton("Modify");
        bnModify.setSize(160,50);
        bnModify.setLocation(100,460);
        bnModify.addActionListener(this);
        c.add(bnModify);

        bnRemove = new JButton("Remove");
        bnRemove.setSize(160,50);
        bnRemove.setLocation(325,390);
        bnRemove.addActionListener(this);
        c.add(bnRemove);

        bnRemoveAll = new JButton("Remove All");
        bnRemoveAll.setSize(160,50);
        bnRemoveAll.setLocation(325,460);
        bnRemoveAll.addActionListener(this);
        c.add(bnRemoveAll);

        save = new JButton("Save");
        save.setSize(160,50);
        save.setLocation(550, 390);
        save.addActionListener(this);
        c.add(save);

        open = new JButton("Open");
        open.setSize(160,50);
        open.setLocation(550, 460);
        open.addActionListener(this);
        c.add(open);

        setSize(800, 600);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setLocation(100,100);
        setVisible(true);
    }

    // Overloaded constructor for the main window with initial data
    public Survey(String fName) {
        super("Survey on Government Shutdown");

        Container c = getContentPane();
        c.setLayout(null);

        // Column Labels
        column1 = new JLabel("Participant No.");
        column1.setFont(new Font("Arial", Font.BOLD, 14));
        column1.setForeground(Color.BLUE);
        column1.setLocation(15, 8);
        column1.setSize(150, 18);
        c.add(column1);

        column2 = new JLabel("Zip Code");
        column2.setFont(new Font("Arial", Font.BOLD, 14));
        column2.setForeground(Color.BLUE);
        column2.setLocation(180, 8);
        column2.setSize(150, 18);
        c.add(column2);

        column3 = new JLabel("Employment");
        column3.setFont(new Font("Arial", Font.BOLD, 14));
        column3.setForeground(Color.BLUE);
        column3.setLocation(333, 8);
        column3.setSize(150, 18);
        c.add(column3);

        column4 = new JLabel("Impact");
        column4.setFont(new Font("Arial", Font.BOLD, 14));
        column4.setForeground(Color.BLUE);
        column4.setLocation(535,8);
        column4.setSize(150,18);
        c.add(column4);

        column5 = new JLabel("Cause");
        column5.setFont(new Font("Arial", Font.BOLD, 14));
        column5.setForeground(Color.BLUE);
        column5.setLocation(680,8);
        column5.setSize(150,18);
        c.add(column5);

        // Set up display area
        sampleArray = new ArrayList<CSample>();
        surveys = new DefaultListModel();
        sampleList = new JList(surveys);
        sampleList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION );
        sampleList.setFont(new Font("Monospaced", Font.PLAIN, 12));
        scrollPane = new JScrollPane(sampleList);
        scrollPane.setSize(765, 350);
        scrollPane.setLocation(10, 30);
        c.add( scrollPane );

        // Buttons
        bnAdd = new JButton("Add");
        bnAdd.setSize(160,50);
        bnAdd.setLocation(100, 390);
        bnAdd.addActionListener(this);
        c.add(bnAdd);

        bnModify = new JButton("Modify");
        bnModify.setSize(160,50);
        bnModify.setLocation(100,460);
        bnModify.addActionListener(this);
        c.add(bnModify);

        bnRemove = new JButton("Remove");
        bnRemove.setSize(160,50);
        bnRemove.setLocation(325,390);
        bnRemove.addActionListener(this);
        c.add(bnRemove);

        bnRemoveAll = new JButton("Remove All");
        bnRemoveAll.setSize(160,50);
        bnRemoveAll.setLocation(325,460);
        bnRemoveAll.addActionListener(this);
        c.add(bnRemoveAll);

        save = new JButton("Save");
        save.setSize(160,50);
        save.setLocation(550, 390);
        save.addActionListener(this);
        c.add(save);

        open = new JButton("Open");
        open.setSize(160,50);
        open.setLocation(550, 460);
        open.addActionListener(this);
        c.add(open);

        boolean runFlag = true;
        // Init new data from fName
        try {
            ObjectInputStream init = new ObjectInputStream(new FileInputStream(fName));
            while (runFlag) {
                surveyNumber++;
                CSample samp = (CSample) init.readObject();
                sampleArray.add(samp);
                surveys.addElement(samp.toString());
                sampleList.setSelectedIndex(surveys.size()-1);
                sampleList.setSelectedIndex(surveys.size()-1);
            }
            init.close();
        } catch (EOFException ex) {
            System.out.println("Initial data EOF reached.");
            runFlag = false;
            surveyNumber--;
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }

        setSize(800, 600);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setLocation(100,100);
        setVisible(true);
    }

    // Handle Button Presses
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==bnAdd) {
            surveyNumber++;
            CSample defaultSample = new CSample(surveyNumber,"",1,1,0);
            SurveyDialog dialogWnd = new SurveyDialog(this, "Add Survey",defaultSample);
            if (!dialogWnd.isCancelled()) {
                sampleArray.add(dialogWnd.getCSample());
                surveys.addElement(dialogWnd.getCSample().toString());
                sampleList.setSelectedIndex(surveys.size()-1);
                sampleList.ensureIndexIsVisible(surveys.size()-1);
            }
        }

        else if(e.getSource()==bnModify) {
            int index = sampleList.getSelectedIndex();
            if (index >= 0) {
                SurveyDialog dialogWnd = new SurveyDialog(this, "Modify Survey", sampleArray.get(index));
                if (!dialogWnd.isCancelled()) {
                    sampleArray.set(index, dialogWnd.getCSample());
                    surveys.set(index, dialogWnd.getCSample().toString());
                }
            }
        }

        else if(e.getSource()==bnRemove) {
            int indexRm = sampleList.getSelectedIndex();
            if (indexRm>=0) {
                sampleArray.remove(indexRm);
                surveys.remove(indexRm);
                if (surveys.size()>0) {
                    if (indexRm == surveys.size()) {
                        sampleList.setSelectedIndex(indexRm);
                        sampleList.ensureIndexIsVisible(indexRm);
                    }
                }
            }
        }

        else if(e.getSource()==bnRemoveAll) {
            sampleArray.clear();
            surveys.clear();
        }

        else if(e.getSource()==save) {
            JFileChooser choose = new JFileChooser();
            choose.showSaveDialog(this);
            try {
                output = new ObjectOutputStream(new FileOutputStream(choose.getSelectedFile() + ".dat"));
                for (int i=0; i<sampleArray.size(); i++) {
                    output.writeObject(sampleArray.get(i));
                }
                output.close();
            } catch (IOException ex) {
                System.out.println(ex.toString());
            }

        }

        else if(e.getSource()==open) {
            boolean runFlag = true;
            JFileChooser chooser = new JFileChooser();
            chooser.showOpenDialog(this);
            sampleArray.clear();
            surveys.clear();
            surveyNumber=0; // reset survey count for new session
            try {
                input = new ObjectInputStream(new FileInputStream(chooser.getSelectedFile()));
                while (runFlag) {
                    surveyNumber++;
                    CSample sample = (CSample) input.readObject();
                    sampleArray.add(sample);
                    surveys.addElement(sample.toString());
                    sampleList.setSelectedIndex(surveys.size()-1);
                    sampleList.setSelectedIndex(surveys.size()-1);
                }
            } catch (EOFException ex) {
                System.out.println("Opened file EOF reached.");
                runFlag = false;
                surveyNumber--; // decrement once since surveyNumber was incremented before EOFException was thrown
            } catch (IOException ex) {
                ex.printStackTrace();
            } catch (ClassNotFoundException ex) {
                ex.printStackTrace();
            }
        }
    }


    public static void main(String[] args) {
        if (args.length >= 1)
            new Survey(args[0]);
        else
            new Survey();
    }
}